def rotate2b(nab, n):
	n %= 8
	nab = ((nab << n) | (nab >> (16-n))) & 0xffff
	a, b = nab>>8, nab&0xff
	return bytearray([a,b])

def mix(a, b, lut):
	x, y = 0, 0
	for i in range(8):
		L = lut[((a>>i)&1)*2+((b>>i)&1)]
		x, y = x | ((L>>1)<<i), y | ((L&1)<<i)
	return x, y

def decrypt_round(data, keyb):
	keyb %= 24
	lut_forward = []
	v = [1, 2, 3, 0]
	lut_forward.append(v.pop(keyb%4))
	lut_forward.append(v.pop(keyb//4%3))
	lut_forward.append(v.pop(keyb//12%2))
	lut_forward.append(v.pop())
	lut_backward = [lut_forward.index(q) for q in range(4)]
	#for a in range(256):
	#	for b in range(256):
	#		A, B = mix(a, b, lut_forward)
	#		assert(mix(A, B, lut_backward) == (a, b))
	print(lut_forward, lut_backward)
	#print(data)
	for i in range(16):
		data[i*2], data[i*2+1] = mix(data[i*2], data[i*2+1], lut_backward)
	#print(data)
	d = bytearray()
	for i in range(16):
		d.append(data[2*i+1])
	for i in range(16):
		d.append(data[30-2*i])

	return d

def decrypt(data, password):
	assert(len(password) == 8)
	assert(len(data) == 32)

	password = [q % 24 for q in password]

	data = bytearray(data)

	for i in range(16):
		data[i*2], data[i*2+1] = rotate2b(data[i*2] + 256*data[i*2+1], password[-1])
	print("Before rot: ", data)
	for i in range(0, 32, 2):
		data[i], data[i+1] = data[i+1], data[i]
		
	for round in range(7,-1,-1):
		print("Round ", round, data.hex())
		if round%2==1:
			data.reverse()
		data = decrypt_round(data, password[round])
		if round%2==1:
			data.reverse()

	return data

"""
hx = [chr(q+48) for q in range(32)]
def test(code):
	data = bytearray(plain)
	stack = []
	while len(code):
		if code[0] == '-':
			data[hx.index(code[1])] = stack.pop()
			#print("pop", bytearray(stack), data)
			code = code[2:]
		elif code[0] == '&':
			#print("mix", bytearray(stack), data)
			b = stack.pop()
			a = stack.pop()
			#print("A = ", a, "B = ", b)
			A,B=mix(a, b, [2, 1, 3, 0])
			stack.append(A)
			stack.append(B)
			code = code[1:]
		elif code[0] in hx:
			stack.append(data[hx.index(code[0])])
			#print("push", bytearray(stack), data)
			code = code[1:]
		else:
			raise ValueError
	#assert(len(stack) == 0)
	return data
"""
#pycrypt = (test("CBKMNLHOIAJ@DFGE?8:67=<>93;1452-4-:-8-2-F-6-B-L-H-J->-<-D-@-ND&-D-E@&-@-AB&-B-CF&-F-GN&-N-O:&-:-;L&-L-M<&-<-=0&-0-1>&->-?6&-6-72&-2-34&-4-58&-8-9J&-J-KH&-H-I"))
#print("compare")
#print(bytearray(crypt))
#print(pycrypt)

#print(decrypt_round(bytearray(pycrypt), ord("0")))

plain = bytearray(b"0123456789:;<=>?@ABCDEFGHIJKLMNO")

#print(decrypt_round(bytearray(bytes.fromhex("7fcf7fce7fcd7fcc7fcb7fca7fc97fc87fc77fc67fc57fc47fc37fc27fc17fc0")), 0))
#print(decrypt_round(bytearray(bytes.fromhex("cf7fce7fcd7fcc7fcb7fca7fc97fc87fc77fc67fc57fc47fc37fc27fc17fc07f")), 1))
#print(decrypt_round(bytearray(bytes.fromhex("de8ddebface09aef93e7afafafe18affade0abeedfb39affababace19affdfaa")), ord('*')))
#print(decrypt_round(bytearray(bytes.fromhex("de8ddebf8ce09aef93e78faf8fe18aff8de08beedfb39aff8bab8ce19affdfaa")), ord('*')))
#print(decrypt_round(bytearray(bytes.fromhex("acd69ebf93b28aab8baadfac91d48ac692e29aea93c69ac8dfab92b29aff8aaa")), ord('^')))

print(decrypt(bytes.fromhex("b87e220a16f4046b6efab290cd8ac6bf44f98a7c6674b1c8d42c8813655e3cec"), b"HelloWRL"))

print(decrypt(bytes.fromhex("5e32c2f904bafe7e0c3f2d951df3b0ff4030649b94d34c3d42067f60f103f65e"), b"HeLloWRL"))

print(decrypt(bytes.fromhex("8ba409960881fbab676e7e4a47447770b365d57c186169286b2f064d0b434bf6"), b"XjyGaWRW"))

luts = []
for i in range(24):
	print(f"Settng up LUT#{i}")
	lut_forward = []
	v = [1, 2, 3, 0]
	lut_forward.append(v.pop(i%4))
	lut_forward.append(v.pop(i//4%3))
	lut_forward.append(v.pop(i//12%2))
	lut_forward.append(v.pop())
	lut_backward = [lut_forward.index(q) for q in range(4)]
	lut_table = [[mix(a, b, lut_backward) for b in range(256)] for a in range(256)]
	luts.append(lut_table)

def decrypt_round_w_lut(data, lut_backward):
	for i in range(16):
		data[i*2], data[i*2+1] = lut_backward[data[i*2]][data[i*2+1]]
	d = bytearray()
	for i in range(16):
		d.append(data[2*i+1])
	for i in range(16):
		d.append(data[30-2*i])

	return d

data = bytearray(bytes.fromhex("8ba409960881fbab676e7e4a47447770b365d57c186169286b2f064d0b434bf6"))
for i in range(16):
	data[i*2+1], data[i*2] = rotate2b(data[i*2] + 256*data[i*2+1], 15)
Data = bytes(data)
for a in range(24):
	for b in range(24):
		print(a, b, (a*24+b)*100//(24*24))
		for c in range(24):
			for d in range(24):
				for e in range(24):
					for f in range(24):
						data = bytearray(Data)

						data.reverse()
						data = decrypt_round_w_lut(data, luts[15])
						data.reverse()
						data = decrypt_round_w_lut(data, luts[f])
						data.reverse()
						data = decrypt_round_w_lut(data, luts[e])
						data.reverse()
						data = decrypt_round_w_lut(data, luts[d])
						data.reverse()
						data = decrypt_round_w_lut(data, luts[c])
						data.reverse()
						data = decrypt_round_w_lut(data, luts[b])
						data.reverse()
						data = decrypt_round_w_lut(data, luts[a])
						data.reverse()
						data = decrypt_round_w_lut(data, luts[16])
						data.reverse()

						if data[0] == 72 and data[1] == 65 and data[2] == 99:
							print(key, data)
# This would exhaust the search space in about 2h in this minimally optimised python
# Since a = 10 is correct, the solution will be found at about the 1h mark
# Multiprocessing should get that to tens of minutes
# Writing it in C should get that even quicker
