%include "termios.inc" 

global _start

%macro mystr 1+
	.len: dw .end - .data
	.data: db %1
	.end:
%endmacro

%macro ioctl 3
	mov rdx, %3
	mov rsi, %2
	mov rdi, %1
	mov rax, 16
	syscall
%endmacro

%define ansi_save		0x1b, "[s"
%define ansi_up(n)		0x1b, "[", n, "A"
%define ansi_right(n)	0x1b, "[", n, "C"

section .data

art1: mystr 10, \
"  ███▄ ▄███▓ ▄▄▄       ███▄ ▄███▓ ▄▄▄          ▄▄▄▄   ▓█████ ▄▄▄       ██▀███", 10, \
" ▓██▒▀█▀ ██▒▒████▄    ▓██▒▀█▀ ██▒▒████▄       ▓█████▄ ▓█   ▀▒████▄    ▓██ ▒ ██▒", 10, \
" ▓██    ▓██░▒██  ▀█▄  ▓██    ▓██░▒██  ▀█▄     ▒██▒ ▄██▒███  ▒██  ▀█▄  ▓██ ░▄█ ▒", 10, \
" ▒██    ▒██ ░██▄▄▄▄██ ▒██    ▒██ ░██▄▄▄▄██    ▒██░█▀  ▒▓█  ▄░██▄▄▄▄██ ▒██▀▀█▄", 10, \
" ▒██▒   ░██▒ ▓█   ▓██▒▒██▒   ░██▒ ▓█   ▓██▒   ░▓█  ▀█▓░▒████▒▓█   ▓██▒░██▓ ▒██▒", 10, \
" ░ ▒░   ░  ░ ▒▒   ▓▒█░░ ▒░   ░  ░ ▒▒   ▓▒█░   ░▒▓███▀▒░░ ▒░ ░▒▒   ▓▒█░░ ▒▓ ░▒▓░", 10, \
" ░  ░      ░  ▒   ▒▒ ░░  ░      ░  ▒   ▒▒ ░   ▒░▒   ░  ░ ░  ░ ▒   ▒▒ ░  ░▒ ░ ▒░", 10, \
" ░      ░     ░   ▒   ░      ░     ░   ▒       ░    ░    ░    ░   ▒     ░░   ░", 10, \
"        ░         ░  ░       ░         ░  ░    ░         ░  ░     ░  ░   ░", 10, \
"                                                    ░", 10, 10

art2a: mystr \
"       ʕ•ᴥ•ʔ        ___________________________", 10, \
"   ___/ \ /\ ___   \                           \", 10, \
"  / `/___Y__Y   \   \  Hello Goldilocks! Let's |", 10, \
"  (  (__ \__)   |    \ do some secret-ing this |", 10, \
"  |  /  \       /    | time. What do you want  |", 10, \
"  \______>-____/     | the password to be?     |", 10, \
"     | ( H ) |       \_________________________/", 10, \
"    /  -----  \"
art2b: mystr   "                                                        ______", 10, \
"                                                                      /      \", 10, \
"                                     ____________________________     | *  * |", 10, \
"                                    /                           /     |  /   |", 10, \
"                                    | The password is ........ /      | ---  |", 10, \
"                                    \_________________________/       \__  _/", 10, \
"                                                                        /  \", 10, \
"                                                                       /    \", 10, ansi_save, ansi_up("4"), ansi_right("54")

art3a: mystr \
"       ʕ•ᴥ•ʔ        __________________________", 10, \
"   ___/ \ /\ ___   \                          \", 10, \
"  / `/___Y__Y   \   \  Very well. Now tell me |", 10, \
"  (  (__ \__)   |    \ your secret and I'll   |", 10, \
"  |  /  \       /    | maul it for you. GRRR! |", 10, \
"  \______>-____/     \________________________/", 10, \
"     | ( H ) |"
art3b: mystr  "                                                         ______", 10, \
"    /  -----  \                                                       /      \", 10, \
"                             ____________________________________     | *  * |", 10, \
"                            /                                   /     |  /   |", 10, \
"                            | ................................ /      | ---  |", 10, \
"                            \_________________________________/       \__  _/", 10, \
"                                                                        /  \", 10, \
"                                                                       /    \", 10, ansi_save, ansi_up("4"), ansi_right("30")

art4: mystr \
"       ʕ•ᴥ•ʔ        _____________________________________", 10, \
"   ___/ \ /\ ___   \                                     \", 10, \
"  / `/___Y__Y   \   \   Haha more like                   |", 10, \
"  (  (__ \__)   |    \  @                                |", 10, \
"  |  /  \       /     | @                                |", 10, \
"  \______>-____/      \__________________________________/", 10, \
"     | ( H ) |", 10, \
"    /  -----  \", 10

hexchars: db "0123456789abcdef"

ansi_restore: mystr 0x1b, "[u"
ansi_hide_cursor: mystr 0x1b, "[?25l"
ansi_show_cursor: mystr 0x1b, "[?25h"

data: db 14, 141, 142, 14, 103, 78, 229, 229, 142, 238, 46, 141, 140, 174, 69, 206, 109, 236, 165, 237, 238, 139, 78, 174, 14, 14, 140, 173, 100, 14, 134, 103
times 43-32 db 0

bytecode_idx: dq bytecode

sleep_tspec:
	.tv_sec: dq 0
	.tv_nsec: dq 0

bytecode:
	.trupplestwitter: db "#0#:#4#8#N#<#L#J#D#@#B#F#2#>#H#6!"
	.round1: db "CBKMNLHOIAJ@DFGE?8:67=<>93;1452-4-:-8-2-F-6-B-L-H-J->-<-D-@-ND&-D-E@&-@-AB&-B-CF&-F-GN&-N-O:&-:-;L&-L-M<&-<-=0&-0-1>&->-?6&-6-72&-2-34&-4-58&-8-9J&-J-KH&-H-I!"
	.round2: db "8:5>;=91?072634<CEM@IBJKLAGNFDH-A-9-=-M-?-3-I-G-E-5-C-1-K-;-77&-7-6G&-G-FI&-I-HC&-C-BK&-K-JA&-A-@O&-O-N1&-1-0M&-M-L=&-=-<5&-5-49&-9-83&-3-2E&-E-D;&-;-:?&-?->!"
	.round3: db "L7>-L->-76<H?N3-6-3-N-?-H-<K9B-K-B-9=J;FCI-=-I-C-F-;-J8@O124-8-4-2-1-O-@M5:DGA-M-A-G-D-:-5A@&-@-AIH&-H-I98&-8-9GF&-F-G;:&-:-;76&-6-7ON&-N-OKJ&-J-K54&-4-5ML&-L-M32&-2-3ED&-D-E10&-0-1?>&->-?=<&-<-=CB&-B-C!"
	.round4: db "C7@1LI-C-I-L-1-@-74F=-4-=-FKG?0NM-K-M-N-0-?-GHA3-H-3-A5D9<6B-5-B-6-<-9-DJE;8>2-J-2->-8-;-EFG&-G-FNO&-O-NBC&-C-BDE&-E-D@A&-A-@67&-7-689&-9-8<=&-=-<JK&-K-J01&-1-0HI&-I-H45&-5-423&-3-2LM&-M-L>?&-?->:;&-;-:!"
	.round5: db "JKFEH@IMCGANLDOB<9;5>728:=463?1-2-N-6-<-8-J-D-@-4->-L-:-F-B-HJ&-J-K0&-0-1F&-F-G6&-6-72&-2-3L&-L-M@&-@-AH&-H-I4&-4-5<&-<-=N&-N-O>&->-?D&-D-EB&-B-C8&-8-9:&-:-;!"
	.round6: db "862:>1?3;=<47950JMFHENDC@ILABKG-?-G-5-3-I-C-1-7-9-M-;-A-=-K-EO&-O-NE&-E-D=&-=-<A&-A-@G&-G-F7&-7-65&-5-49&-9-8I&-I-H1&-1-0M&-M-L3&-3-2;&-;-:K&-K-JC&-C-B?&-?->!"
	.round7: db "L7>-L->-7I=J;FC-I-C-F-;-J-=9BK-9-K-B5:DGAM-5-M-A-G-D-:1248@O-1-O-@-8-4-236<H?N-3-N-?-H-<-610&-0-154&-4-5?>&->-?ON&-N-OGF&-F-GA@&-@-A32&-2-3ED&-D-E;:&-:-;CB&-B-CIH&-H-IKJ&-J-KML&-L-M=<&-<-=98&-8-976&-6-7!"
	.round8: db "<6B5D9-<-9-D-5-B-6HA3-H-3-A4F=-4-=-F7@1LIC-7-C-I-L-1-@G?0NMK-G-K-M-N-0-?>2JE;8->-8-;-E-J-2HI&-I-HJK&-K-J01&-1-0<=&-=-<LM&-M-LFG&-G-F67&-7-645&-5-4@A&-A-@>?&-?->89&-9-8DE&-E-DBC&-C-BNO&-O-N23&-3-2:;&-;-:#2#B#F#4#0#8#>#D#@#H#L#N#:#J#6#<!"

password_current_byte: db 0
password: db 5, 0, 0, 0, 0, 0, 0, 0

mix_lut: db 0, 1, 2, 3

section .text

; void __spoils<r8,rdx,rsi,rdi,rax> __usercall sleep(size_t seconds@<rax>, size_t nanoseconds@<rdx>)
sleep: ret
	mov r8, 0
	cmp rdx, 999999999
	cmova rdx, r8
	mov [sleep_tspec.tv_sec], rax
	mov [sleep_tspec.tv_nsec], rdx
	mov rsi, 0
	mov rdi, sleep_tspec
	mov rax, 35
	syscall
	ret

; void __spoils<rdx,rsi,rdi,rax> __usercall write_mystr(void *str@<rdi>)
write_mystr:
	movzx rdx, word [rdi]
	lea rsi, [rdi+2]
	mov rdi, 0
	mov rax, 1
	syscall
	ret

; void _start()
_start:
	push rbp
	mov rdi, ansi_hide_cursor
	call write_mystr
	mov rdi, art1
	call write_mystr
	
	call jit_run

	mov rax, 1
	mov rdx, 500000000
	call sleep

	mov rdi, art2a
	call write_mystr

	mov rax, 3
	mov rdx, 0
	call sleep

	mov rdi, art2b
	call write_mystr

	mov rdi, ansi_show_cursor
	call write_mystr
	mov rdi, password
	mov rcx, 8
	call read_raw
	mov rdi, ansi_restore
	call write_mystr
	mov rdi, ansi_hide_cursor
	call write_mystr

	mov rax, 1
	mov rdx, 0
	call sleep

	mov rdi, art3a
	call write_mystr

	mov rax, 3
	mov rdx, 0
	call sleep

	mov rdi, art3b
	call write_mystr

	mov rdi, ansi_show_cursor
	call write_mystr
	mov rdi, data
	mov rcx, 32
	call read_raw

	call jit_run
	call jit_run

	mov rdi, ansi_restore
	call write_mystr
	mov rdi, ansi_hide_cursor
	call write_mystr

	call jit_run
	call jit_run
	call jit_run

	mov rax, 1
	mov rdx, 0
	call sleep

	call jit_run
	call jit_run
	call jit_run

	; Find '@'
	mov rdi, art4.data
	mov al, '@'
	mov rcx, -1
	repne scasb
	dec rdi

	; Put the hex representation of the first 16 bytes of `data` at the first @
	mov rsi, data
	mov rcx, 16
	mov rbx, hexchars
	call hexloop

	; Find the second @
	mov al, '@'
	mov rcx, -1
	repne scasb
	dec rdi

	; Put the next 16 bytes' hex representation at the second @
	mov rcx, 16
	call hexloop

	mov rdi, art4
	call write_mystr

	pop rbp
	jmp exit

; void __spoils<rax,rdi,rsi,rcx> __usercall hexloop(char *hex_alphabet@<rbx>, char *input@<rsi>, size_t input_size@<rcx>, char *output@<rdi>)
hexloop:
	xor ah, ah
	lodsb
	ror ax, 4
	xlatb
	stosb
	rol ax, 4
	and al, 15
	xlatb
	stosb
	loop hexloop
	ret

; void __spoils<rdx,rsi,rdi,rax> enable_raw(void)
disable_raw:
	cmp byte [termios_changed], 0
	jz .locret

	mov byte [termios_changed], 0
	; tcsetattr(STDIN_FILENO, TCSAFLUSH, &original_termios);
	ioctl 0, IOCTL_TCSETSF, original_termios
	cmp rax, -1
	jz bad_error

	.locret: ret

; void __spoils<rdx,rax> enable_raw(void)
enable_raw:
	push rcx
	push rdi
	push rsi
	cmp byte [termios_changed], 0
	jnz .skip_orig

	; tcgetattr(STDIN_FILENO, &original_termios);
	ioctl 0, IOCTL_TCGETS, original_termios
	cmp rax, -1
	jz bad_error

	; memcpy(modified_termios, original_termios, sizeof(termios))
	mov rcx, termios_size
	mov rsi, original_termios
	mov rdi, modified_termios
	rep movsb

	.skip_orig:

	; modified_termios.c_lflag &= ~(ICANON);
	mov eax, [modified_termios + termios.c_lflag]
	and eax, ~(ICANON)
	mov [modified_termios + termios.c_lflag], eax

	; tcsetattr(STDIN_FILENO, TCSAFLUSH, &modified_termios);
	ioctl 0, IOCTL_TCSETSF, modified_termios
	cmp rax, -1	
	jz bad_error

	mov byte [termios_changed], 1

	.locret:
	pop rsi
	pop rdi
	pop rcx
	ret

; void __spoils<r14,r15,rcx,rdi,rsi,rax> __usercall read_raw(char *dest@<rdi>, size_t count@<rcx>)
read_raw:
	cmp byte [termios_changed], 0
	jnz .raw_enabled
	call enable_raw
	.raw_enabled:

	.read_loop:
		mov r15, rcx
		mov r14, rdi

		; read(0, dest, 1)
		mov rdx, 1
		mov rsi, rdi
		mov rdi, 0
		mov rax, 0
		syscall

		test rax, rax
		js bad_error

		mov rcx, r15
		mov rdi, r14

		inc rdi
		dec rcx
		jnz .read_loop

	call disable_raw
	ret

be: mystr "Error", 10

bad_error:
	mov rdi, be
	call write_mystr
exit:
	call disable_raw

	mov rdi, 0
	mov rax, 60
	syscall

; void __spoils<r8,r9> __usercall calculate_mix_lut(char keybyte@<al>)
calculate_mix_lut:
	push rbp
	mov rbp, rsp
	sub rsp, 8

	.choicelist equ -8

	mov qword [rbp+.choicelist], 0x0000000000030201

	; r8 = keybyte
	movzx rax, al
	mov r8, rax
	
	; i = keybyte % 4
	and rax, 3

	; shuffle[0] = list[i]
	movzx rbx, al
	mov al, [rbp+.choicelist+rbx]
	mov [mix_lut+0], al

	; del list[rbx]
	mov eax, [rbp+.choicelist+1+rbx]
	mov [rbp+.choicelist+rbx], eax

	; r8 = r8 / 4
	shr r8, 2
	mov rax, r8
	; al = r8 % 3; r8 = r8 / 3
	imul ax, ax, 0x56
	mov al, ah
	movzx rax, al
	mov r9, rax
	lea rax, [rax+2*rax]
	sub rax, r8
	mov r8, r9
	neg rax

	; shuffle[1] = list[rbx=al]
	movzx rbx, al
	mov ah, [rbp+.choicelist+rbx]
	mov [mix_lut+1], ah
	; del list[rbx]
	mov eax, [rbp+.choicelist+1+rbx]
	mov [rbp+.choicelist+rbx], eax

	; shuffle[2] = choicelist[r8&1]
	and r8, 1
	mov al, [rbp+.choicelist+r8]
	mov [mix_lut+2], al

	; shuffle[3] = choicelist[!r8&1]
	xor r8, 1
	mov al, [rbp+.choicelist+r8]
	mov [mix_lut+3], al

	;and dword [mix_lut], 0x03030303

	mov rsp, rbp
	pop rbp
	ret

jit_run:
	push rbp
	mov rbp, rsp

.jit_translate_round:
	; shift one password byte
	mov rax, [password]
	mov [password_current_byte], rax
	call calculate_mix_lut
	mov rsi, [bytecode_idx]

	mov rdi, jit_start
	.jit_translate_loop:
		cmp rdi, jit_boundary
		jae bad_error

		lodsb
		sub al, '!'
		jz .jit_emit_done ; done
		sub al, '&'-'!'
		jz .jit_emit_mix ; done
		sub al, '.'-'&'
		jz .jit_emit_confuse ; done ??
		sub al, '-'-'.'
		jz .jit_emit_pop ; done
		sub al, '#'-'-'
		jz .jit_emit_rot2 ; done
		sub al, '0'-'#'
		cmp al, 43
		jae bad_error

	.jit_emit_push:
		; A0 memaddr mov al, [MEMADDR]
		mov byte [rdi], 0xa0
		inc rdi
		movzx rax, al
		lea rax, [data+rax]
		stosq
		; 48 ff cc dec rsp
		; 88 04 24 mov [rsp], al
		mov rax, 0x240488ccff48
		stosq
		sub rdi, 2

		jmp .jit_translate_loop

	.jit_emit_mix:
		xor al, al
		jz .mix_raw_code_end
		.mix_raw_code:
			pop bx
			mov rcx, 8
			xor rax, rax
			xchg bh, al
			._lp:
				; save a, b
				mov dl, al
				mov dh, bl
				; a&=1; b&=1
				and rax, 1
				and rbx, 1
				; dil = lut[a*2+b]
				mov dil, [mix_lut+2*rax+rbx]

				mov sil, dil ; select bit 1 of LUT result
				shr sil, 1
				mov al, dl ; restore al
				and al, ~1 ; set bit 0 to sil
				or al, sil

				and dil, 1 ; select bit 0 of LUT result
				mov bl, dh ; restore bl
				and bl, ~1 ; set bit 0 to dil
				or bl, dil

				; next bit
				rol al, 1
				rol bl, 1

				loop ._lp  ; repeat 8 times
			mov bh, al
			rol bx, 8
			push bx
		.mix_raw_code_end:

		push rsi
		push rcx

		mov rsi, .mix_raw_code
		mov rcx, .mix_raw_code_end-.mix_raw_code
		rep movsb

		pop rcx
		pop rsi

		jmp .jit_translate_loop

	.jit_emit_pop:
		; 8a 04 24 mov al, [rsp]
		; ff c4 inc esp
		; a2 memaddr mov [MEMADDR], al
		mov rax, 0xa2_c4ff_24048a
		stosq
		sub rdi, 2

		lodsb
		sub al, '0'
		cmp al, 43
		jae bad_error
		movzx rax, al
		lea rax, [data+rax]
		stosq ; MEMADDR

		jmp .jit_translate_loop

	.jit_emit_rot2:
		lodsb
		sub al, '0'
		cmp al, 43
		jae bad_error

		movzx rax, al
		lea rax, [data+rax]
		mov r8, rax ; save memaddr
		; 66 a1 memaddr mov ax, [memaddr]
		mov word [rdi], 0xa166
		add rdi, 2
		stosq

		; 66 c1 c8 nn ror ax, nn
		movzx eax, byte [password_current_byte]
		and al, 7	; if we did a rotate on a byte then there would've been no difference -> a %8 here would be covered by the %24 of mix, but we're rotating 16 bits so this would double the complexity for each password byte (48 instead of 24 values)
		ror eax, 8
		or eax, 0xc8c166
		stosd

		; 66 a3 memaddr mov [memaddr], ax
		mov ax, 0xa366
		stosw
		mov rax, r8	; restore memaddr
		stosq

		jmp .jit_translate_loop

	.jit_emit_confuse:
		rdtsc
		and al, 16*3
		; 9090 nopnop
		; 909f lahf
		; 0c00 or al, 0
		; eb00 jmp 0
		mov rbx, 0x9090_9f90_000c_00eb
		xchg al, cl
		shr rbx, cl
		xchg al, cl
		mov ax, bx
		stosw

		jmp .jit_translate_loop

.jit_emit_done:
	; movabs rax, imm64
	mov ax, 0xb848
	stosw
	mov rax, .jit_run_done
	stosq
	; jmp rax
	mov ax, 0xe0ff
	stosw

	mov [bytecode_idx], rsi

	push rbp
	mov rbp, rsp
	mov rsp, jit_stack_top

	call jit_start
.jit_run_done:

	; jit_start stack cleanup
	mov rsp, rbp
	pop rbp

	; _start stack cleanup
	mov rsp, rbp
	pop rbp

	ret

section .bss
jit_stack: resw 63
jit_stack_top: resw 1
termios_changed: resb 1
original_termios: resb termios_size
modified_termios: resb termios_size
read_byte: resb 1

section .jit write exec
fake_key equ _start
jit_start:
	mov rsi, fake_key
	mov rcx, 32
	mov rdi, data
	.fake_xorloop:
		lodsb
		xor byte [rdi], al
		inc rdi
		loop .fake_xorloop
	ret
resb 4096-$+$$
jit_boundary:
