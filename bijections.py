def name(sx):
	if sx == "0011": sx = "a"
	if sx == "1100": sx = "!a"
	if sx == "0101": sx = "b"
	if sx == "1010": sx = "!b"
	if sx == "0110": sx = "xor"
	if sx == "1001": sx = "xnor"
	return sx

for a0 in range(4):
	for a1 in range(3):
		for a2 in range(2):
			L = [1, 2, 3, 0]
			
			r0 = L.pop(a0)
			r1 = L.pop(a1)
			r2 = L.pop(a2)
			r3 = L.pop()

			R = [r0, r1, r2, r3]
			X = [r0&1, r1&1, r2&1, r3&1]
			Y = [r0>>1, r1>>1, r2>>1, r3>>1]
			sx = ''.join(str(q) for q in X)
			sy = ''.join(str(q) for q in Y)
			sx = name(sx)
			sy = name(sy)

			print(f"#{a0*6+a1*2+a2} {sx}/{sy}")