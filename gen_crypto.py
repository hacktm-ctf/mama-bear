import random

left_cycles = [(1,31,16,8,4,2),(3,30,15,24,12,6),(5,29,17,23,20,10),(7,28,14),(9,27,18),(11,26,13,25,19,22)]
right_cycles = [(0,15,23,27,29,30),(1,16,7,19,25,28),(2,14,8,11,21,26),(3,17,24),(4,13,22),(5,18,6,12,9,20)]

hx = [chr(q+48) for q in range(32)]

def test(code):
	data = list(hx) # copy
	stack = []
	while len(code):
		if code[0] == '-':
			data[hx.index(code[1])] = stack.pop()
			#print("pop", stack, data)
			code = code[2:]
		elif code[0] == '&':
			#print("mix", stack, data)
			b = stack.pop()
			a = stack.pop()
			stack.append(f"X({a}{b})")
			stack.append(f"Y({a}{b})")
			code = code[1:]
		elif code[0] in hx:
			stack.append(data[hx.index(code[0])])
			#print("push", stack, data)
			code = code[1:]
		else:
			raise ValueError
	assert(len(stack) == 0)
	return data


for round in range(8):
	code = ""

	def push(i):
		global code
		code += hx[i]

	def pop(i):
		global code
		code += "-" + hx[i]

	def mix():
		global code
		code += "&"

	impl = round//2%2
	if impl == 0:
		if round%2 == 0:
			v = [i for i in range(16)]
			#random.shuffle(v)
			for i in v:
				push(16+i)
			w = [i for i in range(1, 16)]
			#random.shuffle(w)
			for i in w:
				push(i)
			for i in w[::-1]:
				pop(2*i)
			for i in v[::-1]:
				push(30-2*i)
				mix()
				pop(30-2*i)
				pop(31-2*i)
		else:
			v = [i for i in range(16)]
			random.shuffle(v)
			for i in v:
				push(15-i)
			w = [i for i in range(1, 16)]
			random.shuffle(w)
			for i in w:
				push(31-i)
			for i in w[::-1]:
				pop(31-2*i)
			for i in v[::-1]:
				push(1+2*i)
				mix()
				pop(1+2*i)
				pop(2*i)
	if impl == 1:
		cycles = left_cycles if round%2 == 0 else right_cycles
		random.shuffle(cycles)
		for cyc in cycles:
			n = random.randrange(len(cyc))
			cyc = cyc[n:] + cyc[:n]
			cyc = cyc[::-1]
			for e in cyc:
				push(e)
			pop(cyc[0])
			for e in cyc[1:][::-1]: pop(e)
		v = [i for i in range(16)]
		random.shuffle(v)
		for i in v:
			a, b = 2*i+1, 2*i
			if round%2:
				a, b = b, a
			push(a)
			push(b)
			mix()
			pop(b)
			pop(a)

	if round % 2 == 0:
		assert(test(code) == ['Y(O0)', 'X(O0)', 'Y(N1)', 'X(N1)', 'Y(M2)', 'X(M2)', 'Y(L3)', 'X(L3)', 'Y(K4)', 'X(K4)', 'Y(J5)', 'X(J5)', 'Y(I6)', 'X(I6)', 'Y(H7)', 'X(H7)', 'Y(G8)', 'X(G8)', 'Y(F9)', 'X(F9)', 'Y(E:)', 'X(E:)', 'Y(D;)', 'X(D;)', 'Y(C<)', 'X(C<)', 'Y(B=)', 'X(B=)', 'Y(A>)', 'X(A>)', 'Y(@?)', 'X(@?)'])
	else:
		assert(test(code) == ['X(?@)', 'Y(?@)', 'X(>A)', 'Y(>A)', 'X(=B)', 'Y(=B)', 'X(<C)', 'Y(<C)', 'X(;D)', 'Y(;D)', 'X(:E)', 'Y(:E)', 'X(9F)', 'Y(9F)', 'X(8G)', 'Y(8G)', 'X(7H)', 'Y(7H)', 'X(6I)', 'Y(6I)', 'X(5J)', 'Y(5J)', 'X(4K)', 'Y(4K)', 'X(3L)', 'Y(3L)', 'X(2M)', 'Y(2M)', 'X(1N)', 'Y(1N)', 'X(0O)', 'Y(0O)'])

	if round == 7:
		v = [i for i in range(16)]
		random.shuffle(v)
		for i in v:
			code += "#" + hx[i*2]
	print(f'.round{round+1}: db "{code}!"')
